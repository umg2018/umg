package DecimoTercero;

import java.util.Scanner;

public class DecimoTercero {

    public static void main(String[] args) {
        int menu1, opc = 0;
        double sum, rest, div, mul, num1, num2;
        Scanner leer = new Scanner(System.in);
        System.out.println("BIENVENIDO A LA CALCULADORA UMG");
        System.out.println("Ingrese el primer numero");
        num1 = leer.nextDouble();
        do {
            System.out.println("Que operación desea realizar\n1.Suma \n2.Resta \n3.Multiplicación \n4División");
            menu1 = leer.nextInt();

            switch (menu1) {
                case 1:

                    System.out.println("Ingrese el segundo numero");
                    num2 = leer.nextDouble();
                    sum = num1 + num2;
                    num1 = sum;
                    System.out.println("El resultado es " + sum);
                    System.out.println("Desea realizar otra operación con el resultado? \n1.Si \n2.No");
                    opc = leer.nextInt();
                    break;
                case 2:

                    System.out.println("Ingrese el segundo numero");
                    num2 = leer.nextDouble();
                    rest = num1 - num2;
                    num1 = rest;
                    System.out.println("El resultado es " + rest);
                    System.out.println("Desea realizar otra operación con el resultado? \n1.Si \n2.No");
                    opc = leer.nextInt();
                    break;
                case 3:
                    System.out.println("Ingrese el segundo numero");
                    num2 = leer.nextDouble();
                    mul = num1 * num2;
                    num1 = mul;
                    System.out.println("El resultado es " + mul);
                    System.out.println("Desea realizar otra operación con el resultado? \n1.Si \n2.No");
                    opc = leer.nextInt();
                    break;
                case 4:
                    System.out.println("Ingrese el segundo numero");
                    num2 = leer.nextDouble();
                    div = num1 / num2;
                    num1 = div;
                    System.out.println("El resultado es " + div);
                    System.out.println("Desea realizar otra operación con el resultado? \n1.Si \n2.No");
                    opc = leer.nextInt();
                default:
                    System.out.println("La opcion escrita no es correcta");
            } 
        } while (opc == 1);

    }

}
